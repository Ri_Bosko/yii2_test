<?php

declare(strict_types=1);

namespace app\services;

use app\jobs\ContactPlacementApplicationJob;
use app\jobs\DescriptivePlacementApplicationJob;
use app\models\entities\Post;
use app\models\entities\DescriptivePost;
use app\models\entities\ContactPost;
use app\models\entities\PostQueue;
use Yii;

class PostService implements PostServiceInterface
{
    private function createPostRecord(array $attributes): int
    {
        $post = new Post([
            'type' => $attributes['type'],
            'company_name' => $attributes['company_name'],
            'position' => $attributes['position']
        ]);

        $post->save();

        return $post->id;
    }

    private function createPostQueueRecord(array $attributes, int $postId): void
    {

        $postQueue = new PostQueue([
            'post_id' => $postId,
            'post_at' => $attributes['post_at']
        ]);

        $postQueue->save();
    }

    public function runDescriptiveAction(array $attributes): void
    {
        $postId = $this->createPostRecord($attributes);

        $this->createPostQueueRecord($attributes, $postId);

        $descriptivePost = new DescriptivePost([
            'post_id' => $postId,
            'salary' => $attributes['salary'],
            'position_description' => $attributes['position_description'],
            'starts_at' => $attributes['starts_at'],
            'ends_at' => $attributes['ends_at']
        ]);

        $descriptivePost->save();

        Yii::$app->queue->push(new DescriptivePlacementApplicationJob(['attributes'=>$attributes]));
    }

    public function runContactAction(array $attributes): void
    {
        $postId = $this->createPostRecord($attributes);

        $this->createPostQueueRecord($attributes, $postId);

        $contactPost = new ContactPost([
            'post_id' => $postId,
            'contact_name' => $attributes['contact_name'],
            'contact_email' => $attributes['contact_email'],
        ]);

        $contactPost->save();

        Yii::$app->queue->push(new ContactPlacementApplicationJob(['attributes'=>$attributes]));
    }
}
