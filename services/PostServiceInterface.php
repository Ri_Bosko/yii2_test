<?php

declare(strict_types=1);

namespace app\services;

interface PostServiceInterface
{
    public function runDescriptiveAction(array $attributes): void;
    public function runContactAction(array $attributes): void;
}
