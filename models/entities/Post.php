<?php

namespace app\models\entities;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property int $type
 * @property string $company_name
 * @property string $position
 *
 * @property ContactPost $contactPost
 * @property DescriptivePost $descriptivePost
 * @property PostQueue $postQueue
 */
class Post extends \yii\db\ActiveRecord
{
    const POST_TYPE_DESCRIPTIVE = 0;
    const POST_TYPE_CONTACT = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'company_name', 'position'], 'required'],
            [['type'], 'integer'],
            [['company_name', 'position'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'company_name' => 'Company Name',
            'position' => 'Position',
        ];
    }

    /**
     * Gets query for [[ContactPost]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContactPost()
    {
        return $this->hasOne(ContactPost::className(), ['post_id' => 'id']);
    }

    /**
     * Gets query for [[DescriptivePost]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDescriptivePost()
    {
        return $this->hasOne(DescriptivePost::className(), ['post_id' => 'id']);
    }

    /**
     * Gets query for [[PostQueue]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPostQueue()
    {
        return $this->hasOne(PostQueue::className(), ['post_id' => 'id']);
    }
}
