<?php

namespace app\models;

use yii\base\Model;

class Form2 extends Model //contact post
{
    public $type;
    public $company_name;
    public $position;
    public $contact_name;
    public $contact_email;
    public $post_at;

    public function rules()
    {
        return [
            [['type', 'company_name', 'position', 'contact_email'], 'required'],
            ['type', 'integer'],
            ['contact_name', 'string'],
            ['contact_email', 'email'],
            ['post_at', 'date', 'format' => 'php:Y-m-d H:i:s', 'message' => 'Формат должен быть Y-m-d H:i:s'],
            ['post_at', 'compare', 'compareValue' => date('Y-m-d H:i:s'), 'operator' => '>', 'type' => 'datetime'],
            ['post_at', 'default', 'value' => date('Y-m-d  H:i:s')],
        ];
    }
    public function attributeLabels()
    {
        return [
            'company_name' => 'Название компании',
            'position' => 'Должность',
            'contact_name' => 'Контактное имя',
            'contact_email' => 'Контактный email',
            'post_at' => 'Дата размещения'

        ];
    }
}
