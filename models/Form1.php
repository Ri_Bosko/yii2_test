<?php

namespace app\models;

use yii\base\Model;

class Form1 extends Model //descriptive post
{
    public $type;
    public $company_name;
    public $position;
    public $position_description;
    public $salary;
    public $starts_at;
    public $ends_at;
    public $post_at;

    public function rules()
    {
        return [
            [['type', 'company_name', 'position', 'ends_at'], 'required'],
            ['type', 'integer'],
            ['position_description', 'string'],
            ['salary', 'number', 'message' => 'Размер заработной платы должен быть числом'],
            [['starts_at', 'ends_at'], 'date', 'format' => 'php:Y-m-d', 'message' => 'Формат должен быть Y-m-d'],
            ['post_at', 'date', 'format' => 'php:Y-m-d H:i:s', 'message' => 'Формат должен быть Y-m-d H:i:s'],
            ['post_at', 'compare', 'compareValue' => date('Y-m-d H:i:s'), 'operator' => '>', 'type' => 'datetime'],
            ['starts_at', 'default', 'value' => date('Y-m-d')],
            ['post_at', 'default', 'value' => date('Y-m-d H:i:s')],
            ['ends_at', 'checkDateRange'],

            ];
    }

    public function attributeLabels()
    {
        return [
            'company_name' => 'Название компании',
            'position' => 'Должность',
            'position_description' => 'Описание должности',
            'salary' => 'Размер заработной платы',
            'starts_at' => 'Дата начала',
            'ends_at' => 'Дата окончания',
            'post_at' => 'Дата размещения'

        ];
    }

    public function checkDateRange($attribute, $params)
    {
        if (!($this->ends_at > date('Y-m-d', strtotime('+3 months', strtotime($this->starts_at))))) {
            $this->addError($attribute, 'минимальный период даты начала и даты окончания - 3 месяца');
        }
    }

}
