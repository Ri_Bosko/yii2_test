<?php

declare(strict_types=1);

namespace app\controllers;

use app\models\Form1;
use app\models\Form2;
use app\services\PostService;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotAcceptableHttpException;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;
use yii\widgets\ActiveForm;

class AjaxController extends Controller
{
    public function actionIndex()
    {
        throw new HttpException(404);
    }

    public function actionForm1()
    {
        $form = new Form1();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            // данные в $form удачно проверены
            // запись данных с формы в бд
            // создание задания очереди, отправка письма на почту админу

            $postService = new PostService();
            $postService->runDescriptiveAction($form->getAttributes());

            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'success' => true
            ];
        }
    }

    public function actionForm2()
    {
        $form = new Form2();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            // данные в $form удачно проверены
            // запись данных с формы в бд
            // создание задания очереди, отправка письма на почту админу

            $postService = new PostService();
            $postService->runContactAction($form->getAttributes());

            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'success' => true
            ];
        }
    }

    public function actionValidationForm1()
    {
        $form1 = new Form1();
        if (Yii::$app->request->isAjax && $form1->load(Yii::$app->
            request->post())) {
            Yii::$app->response->format = 'json';

            return ActiveForm::validate($form1);
        }
    }

    public function actionValidationForm2()
    {
        $form2 = new Form2();
        if (Yii::$app->request->isAjax && $form2->load(Yii::$app->
            request->post())) {
            Yii::$app->response->format = 'json';

            return ActiveForm::validate($form2);
        }
    }
}
