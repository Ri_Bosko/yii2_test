create table if not exists post
(
    id int auto_increment
        primary key,
    type smallint not null,
    company_name varchar(50) not null,
    position varchar(50) not null
)
    charset=utf8;

create table if not exists contact_post
(
    id int auto_increment
        primary key,
    post_id int null,
    contact_name varchar(50) default 'contact_name' not null,
    contact_email varchar(50) default 'contact_email' not null,
    constraint contact_post_contact_email_uindex
        unique (contact_email),
    constraint contact_post_post_id_uindex
        unique (post_id),
    constraint contact_post_post_id_fk
        foreign key (post_id) references post (id)
            on delete cascade
)
    charset=utf8;

create table if not exists descriptive_post
(
    id int auto_increment
        primary key,
    post_id int not null,
    salary int not null,
    position_description varchar(255) null,
    ends_at datetime default CURRENT_TIMESTAMP not null,
    starts_at datetime default CURRENT_TIMESTAMP not null,
    constraint descriptive_post_post_id_uindex
        unique (post_id),
    constraint descriptive_post_post_id_fk
        foreign key (post_id) references post (id)
            on delete cascade
)
    charset=utf8;

create table if not exists post_queue
(
    id int auto_increment
        primary key,
    post_id int not null,
    post_at datetime default CURRENT_TIMESTAMP not null,
    notification_sent_at datetime null,
    constraint post_queue_post_id_uindex
        unique (post_id),
    constraint post_queue_post_id_fk
        foreign key (post_id) references post (id)
            on delete cascade
)
    charset=utf8;

