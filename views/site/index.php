<?php
use yii\bootstrap\Tabs;

?>
<?php
echo Tabs::widget([
    'items' => [

        [
            'label' => 'Тип формы',
            'items' => [
                [
                    'label' => 'форма1',
                    'content' => $this->render('forms/form1', ['formModel' => $form1]),
                    'active' => true
                ],
                [
                    'label' => 'форма2',
                    'content' => $this->render('forms/form2', ['formModel' => $form2])
                ],

            ]
        ]
    ]
]);
?>

