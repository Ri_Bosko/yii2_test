<?php

use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\entities\Post;

?>

<?php $activeForm = ActiveForm::begin([
    'id' => 'form2',
    'enableAjaxValidation' => true,
    'validationUrl' => Url::toRoute('ajax/validation-form2')
]); ?>
<?= $activeForm->field($formModel, 'type')->hiddenInput([
    'value' => Post::POST_TYPE_CONTACT
])
    ->label(false); ?>
<?= $activeForm->field($formModel, 'company_name') ?>
<?= $activeForm->field($formModel, 'position') ?>
<?= $activeForm->field($formModel, 'contact_name') ?>
<?= $activeForm->field($formModel, 'contact_email') ?>
<?= $activeForm->field($formModel, 'post_at')->widget(
    DateTimePicker::class,
    [
        'options' => ['placeholder' => $formModel->getAttributeLabel('post_at')],
        'convertFormat' => true,
        'pluginOptions' => [
            'format' => 'yyyy-MM-dd HH:mm:00',
            'startDate' => date('Y-m-d H:i:s'),
            'todayHighlight' => true
        ]
    ]
);?>

    <div class="form-group">
        <?= Html::submitButton('Отправить', ['btn btn-primary']); ?>
    </div>

<?php ActiveForm::end(); ?>

    <div id="response2">
        <p>Данные успешно отправлены</p>
    </div>


<?php
$js = <<<JS
$('#response2').hide();
    $('#form2').on('beforeSubmit', function(){
       var data = $(this).serialize();
        $.ajax({
            url: '/ajax/form2',
            type: 'POST',
            data: data,
            success: function(response){
                $('#response2').fadeIn(500);
                $('#form2').trigger("reset");
                $('#response2').fadeOut(15000);
                //console.log(res);
            },
            error: function(){
                alert('Error!');
            }
        });
        return false;
    });
JS;
$this->registerJs($js);
?>