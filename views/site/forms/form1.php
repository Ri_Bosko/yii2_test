<?php

use app\jobs\DescriptivePlacementApplicationJob;
use app\models\Form1;
use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\entities\Post;

?>

<?php $activeForm = ActiveForm::begin([
    'id' => 'form1',
    'enableAjaxValidation' => true,
    'validationUrl' => Url::toRoute('ajax/validation-form1')
]); ?>
<?= $activeForm->field($formModel, 'type')->hiddenInput([
    'value' => Post::POST_TYPE_DESCRIPTIVE
])
    ->label(false); ?>
<?= $activeForm->field($formModel, 'company_name') ?>
<?= $activeForm->field($formModel, 'position') ?>
<?= $activeForm->field($formModel, 'position_description') ?>
<?= $activeForm->field($formModel, 'salary') ?>
<?= $activeForm->field($formModel, 'starts_at') ?>
<?= $activeForm->field($formModel, 'ends_at') ?>
<?= $activeForm->field($formModel, 'post_at')->widget(
    DateTimePicker::class,
    [
        'options' => ['placeholder' => $formModel->getAttributeLabel('post_at')],
        'convertFormat' => true,
        'pluginOptions' => [
            'format' => 'yyyy-MM-dd HH:mm:00',
            'startDate' => date('Y-m-d H:i:s'),
            'todayHighlight' => true
        ]
    ]
); ?>

    <div class="form-group">
        <?= Html::submitButton('Отправить', ['btn btn-primary']); ?>
    </div>

<?php ActiveForm::end();?>

    <div id="response1">
        <p>Данные успешно отправлены</p>
    </div>


<?php
$js = <<<JS
$('#response1').hide();
    $('#form1').on('beforeSubmit', function(){
       var data = $(this).serialize();
        $.ajax({
            url: '/ajax/form1',
            type: 'POST',
            data: data,
            success: function(response){
                $('#response1').fadeIn(500);  
                $('#form1').trigger("reset");
                $('#response1').fadeOut(15000);
                //console.log(response1);
            },
            error: function(){
                alert('Error!');
            }
        });
        return false;
    });
JS;
$this->registerJs($js);
?>
