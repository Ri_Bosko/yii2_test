<?php

namespace app\jobs;

//use Exception;
use Yii;
use yii\base\BaseObject;
use yii\queue\ExecEvent;
use yii\queue\file\Queue;
use yii\queue\JobInterface;

class DescriptivePlacementApplicationJob extends BaseObject implements JobInterface
{
    public $attributes;

    public function execute($queue)
    {
        Yii::$app->queue->on(Queue::EVENT_AFTER_ERROR, function (ExecEvent $event) {
            if ($event->job instanceof DescriptivePlacementApplicationJob) {
                $event->retry = ($event->attempt <= 3);
                if (!$event->retry){
                    Yii::$app->mailer->compose()
                        ->setFrom(Yii::$app->params['senderEmail'])
                        ->setTo(Yii::$app->params['adminEmail'])
                        ->setSubject('Shit happens')
                        ->setHtmlBody($event->error)
                        ->send();
                }
            }
        });

//        throw new Exception();

        $attributes = $this->attributes;
        unset($attributes['type']);

        Yii::$app->mailer->compose()
            ->setFrom(Yii::$app->params['senderEmail'])
            ->setTo(Yii::$app->params['adminEmail'])
            ->setSubject('Descriptive-Placement-Job')
            ->setHtmlBody($this->mailBbody('<br>', $attributes, ' - '))
            ->send();
    }

    public function mailBbody($glue, $array, $symbol = '=')
    {
        return implode($glue, array_map(
                function ($k, $v) use ($symbol) {
                    return $k . $symbol . $v;
                },
                array_keys($array),
                array_values($array)
            )
        );
    }
}
