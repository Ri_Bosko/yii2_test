<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=10.1.1.3;dbname=yii_test',
    'username' => 'root',
    'password' => 'mysql',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
